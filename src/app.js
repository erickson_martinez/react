'use strict'

import React from 'react'
import { render } from 'react-dom'

const Title = React.createClass({
    render: function () {
        return (
        <div>
            <h1>Estilus Modas</h1>
            <h2>Exclusiva da cabeça aos pés</h2>
        </div>)
    }
})

export default Title