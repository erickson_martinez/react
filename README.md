```bash
$ Projeto React + WebPak 

$ webpack # executar webpak para gerar bundler
$ webpack-dev-server # subir Servidor
```
npm install --save-dev webpack@1
```

Instale também o `webpack` de forma global, para usar o comando `webpack` no seu terminal:

```
npm install -g webpack@1
```

$ webpack-dev-server # servidor assintindo alterações no código

```
npm install --save-dev webpack-dev-server@1
```

Instale também o `webpack-dev-server` de forma global, para usar o comando `webpack-dev-server` no seu terminal:

```
npm install -g webpack-dev-server@1
```
```

```
$ instalando dependencia do react e reactDom
```
npm install --save react@15.4 react-dom@15.4
```

dependências do babel:

```
npm install --save-dev babel-core@6 babel-loader@6 babel-preset-es2015@6 babel-preset-stage-0@6
```
babel-preset-react:

```
npm install --save-dev babel-preset-react@6
```

```